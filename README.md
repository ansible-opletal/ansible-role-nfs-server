ansible-role-nfs-server
=========

This role installs and configures a NFS server

Requirements
------------

None.

Role Variables
--------------

All vars can be found under vars/main.yml

- nfs_mount: "/mnt/nfs1" - Where the NFS export mount will reside
- nfs_exposure: "10.150.102.0/24" - Where from will the NFS be accessible ("*" for any)
- nfs_options: "rw,sync" - Export options

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      vars:
        - nfs_mount: "/mnt/nfs1"
        - nfs_exposure: "*"
        - nfs_options: "rw,sync"
      roles:
         - ansible-role-nfs-server

License
-------

MIT
